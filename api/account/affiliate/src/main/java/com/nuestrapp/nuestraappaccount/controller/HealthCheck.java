package com.nuestrapp.nuestraappaccount.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/api/accounts/affiliate/health-check")
public class HealthCheck {

  public ResponseEntity<String> healthCheck() {
    return new ResponseEntity("Hello! i'm OK", HttpStatus.OK);
  }
}
