package com.nuestrapp.nuestraappaccount.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Class Account Service Interceptor App Config Implements Interceptor.
 *
 * @author Isaias Gabriel Ruza Materano
 * @version 20210129
 * @since 1.0
 * @see com.nuestrapp.nuestraappaccount.interceptor
 */
@Component
public class AccountServiceInterceptorAppConfig implements WebMvcConfigurer {

  @Autowired
  AccountServiceInterceptor accountServiceInterceptor;

  /**
   * Add Interceptors.
   *
   * @return Boolean Interceptor.
   * @exception Exception Fails Internal Server.
   * @see java.lang.String#Value()
   */
  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(accountServiceInterceptor);
  }
}