package com.nuestrapp.nuestraappaccount.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.nuestrapp.nuestraappaccount.NuestraappAccountsApplication;
import com.nuestrapp.nuestraappaccount.domain.UsersAccount;
import com.nuestrapp.nuestraappaccount.domain.Header;
import com.nuestrapp.nuestraappaccount.exception.AccountBadRequestException;
import com.nuestrapp.nuestraappaccount.process.AccountProcess;

/**
 * Class Implments Service for services accounts.
 *
 * @author Isaias Gabriel Ruza Materano
 * @version 20210129
 * @since 1.0
 * @see com.nuestrapp.nuestraappaccount.service
 */
@Service
@Transactional("transactionManager")
public class AccountServiceImpl implements AccountService {

  private static final Logger logger = LogManager
      .getLogger(NuestraappAccountsApplication.class);

  @Autowired
  private AccountProcess accountProcess;

  /**
   * Validate Credencials Header.
   *
   * @return Objet.
   * @exception Exception Fails Internal Server.
   * @see java.lang.Character#charValue()
   */
  @Override
  public Boolean validateCredencial(Header header) throws Exception {

    if (header.getClientId().equals("yappi-123")
        && header.getClientSecret().equals("yappi-321"))
      return true;

    return false;

  }

  /**
   * Validate Token.
   *
   * @return Objet.
   * @exception Exception Fails Internal Server.
   * @see java.lang.Character#charValue()
   */
  @Override
  public Boolean validateToken(Header header) throws Exception {
    // TODO Auto-generated method stub

    if (header.getAuthorization() == null
        || header.getAuthorization().isEmpty())
      throw new AccountBadRequestException("Write token");

    return true;
  }

  /**
   * Affiliate user and account.
   *
   * @return Objeto de Account.
   * @exception Exception Fails Internal Server.
   * @see java.lang.Character#charValue()
   */
  @Override
  public UsersAccount affiliate(UsersAccount userAccount) throws Exception {
    // TODO Auto-generated method stub
    logger.info("Start AccountService - affiliate");

    if (userAccount.getAccount() == null
        || userAccount.getAccount().getAccountNumber() == null
        || userAccount.getAccount().getAccountNumber().isEmpty()
        || userAccount.getAccount().getAccountStatus() == null
        || userAccount.getAccount().getAccountStatus().isEmpty()
        || userAccount.getAccount().getBalance() == null
        || userAccount.getAccount().getAccountType() == null
        || userAccount.getAccount().getAccountType().isEmpty()
        || userAccount.getUser() == null
        || userAccount.getUser().getIdentityId() == null
        || userAccount.getUser().getIdentityId().isEmpty()
        || userAccount.getUser().getFirstName() == null
        || userAccount.getUser().getFirstName().isEmpty()
        || userAccount.getUser().getLastName() == null
        || userAccount.getUser().getLastName().isEmpty()
        || userAccount.getUser().getIdentityType() == null
        || userAccount.getUser().getIdentityType().isEmpty()
        || userAccount.getUser().getPhoneNumber() == null
        || userAccount.getUser().getPhoneNumber().isEmpty()
        || userAccount.getUser().getEmail() == null
        || userAccount.getUser().getEmail().isEmpty()
        || userAccount.getUser().getPassword() == null
        || userAccount.getUser().getPassword().isEmpty())
      throw new AccountBadRequestException("Data" + userAccount);

    return accountProcess.affiliate(userAccount);

  }

}
