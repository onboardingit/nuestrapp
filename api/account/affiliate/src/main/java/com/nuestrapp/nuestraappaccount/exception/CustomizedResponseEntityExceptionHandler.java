package com.nuestrapp.nuestraappaccount.exception;

import java.util.Date;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Class Implments Service for services accounts.
 *
 * @author Isaias Gabriel Ruza Materano
 * @version 20210129
 * @since 1.0
 * @see com.nuestrapp.nuestraappaccount.service
 */
@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler
    extends ResponseEntityExceptionHandler {

  /**
   * Handle All Exceptions.
   *
   * @return ResponseEntity.
   * @exception Exception Fails Internal Server.
   * @see java.lang.Character#charValue()
   */
  @ExceptionHandler(Exception.class)
  public final ResponseEntity<ExceptionResponse> handleAllExceptions(
      Exception ex, WebRequest request) {
    ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(),
        ex.getMessage(), request.getDescription(false));
    return new ResponseEntity<>(exceptionResponse,
        HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * Handle Not Found Exception.
   *
   * @return ResponseEntity.
   * @exception Exception Not Found.
   * @see java.lang.Character#charValue()
   */
  @ExceptionHandler(AccountNotFoundException.class)
  public final ResponseEntity<ExceptionResponse> handleNotFoundException(
      AccountNotFoundException ex, WebRequest request) {
    ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(),
        ex.getMessage(), request.getDescription(false));
    return new ResponseEntity<>(exceptionResponse, HttpStatus.NOT_FOUND);
  }

  /**
   * Handle Bad Request Exception.
   *
   * @return ResponseEntity.
   * @exception Exception Bad Request.
   * @see java.lang.Character#charValue()
   */
  @ExceptionHandler(AccountBadRequestException.class)
  public final ResponseEntity<ExceptionResponse> handleBadRequestException(
      AccountBadRequestException ex, WebRequest request) {
    ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(),
        ex.getMessage(), request.getDescription(false));
    return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
  }

  /**
   * Handle Conflict Exception.
   *
   * @return ResponseEntity.
   * @exception Exception Bad Request.
   * @see java.lang.Character#charValue()
   */
  @ExceptionHandler(AccountConflictException.class)
  public final ResponseEntity<ExceptionResponse> handleConflictException(
      AccountConflictException ex, WebRequest request) {
    ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(),
        ex.getMessage(), request.getDescription(false));
    return new ResponseEntity<>(exceptionResponse, HttpStatus.CONFLICT);
  }

  /**
   * Handle Forbidden Exception.
   *
   * @return ResponseEntity.
   * @exception Exception Bad Request.
   * @see java.lang.Character#charValue()
   */
  @ExceptionHandler(AccountForbiddenException.class)
  public final ResponseEntity<ExceptionResponse> handleForbiddenException(
      AccountForbiddenException ex, WebRequest request) {
    ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(),
        ex.getMessage(), request.getDescription(false));
    return new ResponseEntity<>(exceptionResponse, HttpStatus.FORBIDDEN);
  }
}
