package com.nuestrapp.nuestraappaccount.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Class Implements Runtime Exception Forbidden.
 *
 * @author Isaias Gabriel Ruza Materano
 * @version 20210129
 * @since 1.0
 * @see com.nuestrapp.nuestraappaccount.exception
 */
@ResponseStatus(HttpStatus.FORBIDDEN)
public class AccountForbiddenException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  /**
   * Constructor Account Forbidden Exception.
   *
   * @exception Exception Fails Internal Server.
   * @see java.lang.String#charValue()
   */
  public AccountForbiddenException(String exception) {
    super(exception);
  }

}