package com.nuestrapp.nuestraappaccount.controller;

import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.nuestrapp.nuestraappaccount.NuestraappAccountsApplication;
import com.nuestrapp.nuestraappaccount.exception.AccountForbiddenException;
import com.nuestrapp.nuestraappaccount.domain.UsersAccount;
import com.nuestrapp.nuestraappaccount.domain.Header;
import com.nuestrapp.nuestraappaccount.service.AccountService;

/**
 * Controllador de Rest para servicios de accounts.
 *
 * @author Isaias Gabriel Ruza Materano
 * @version 20210129
 * @since 1.0
 * @see com.nuestrapp.nuestraappaccount.controller
 */

@RestController
@RequestMapping("/api/accounts")
public class AccountController {

  /**
   * Logger de app.
   */
  private static final Logger LOGGER = LogManager
      .getLogger(NuestraappAccountsApplication.class);

  /**
   * Inyección de servicio.
   *
   * @author iruza
   */
  @Autowired
  private AccountService accountService;

  /**
   * Affiliate user and account.
   *
   * @RequestHeader String client-id
   * @RequestHeader String client-secret
   * @return Objeto de Account.
   * @exception Exception Fails Internal Server.
   * @see java.lang.Character#charValue()
   */
  @CrossOrigin
  @PostMapping("affiliate")

  public Map<String, Object> affiliate(
      @RequestBody final UsersAccount userAccount,
      @RequestHeader(name = "client-id", required = true) String clientId,
      @RequestHeader(name = "client-secret", required = true) String clientSecret)
      throws Exception {

    LOGGER.info("Start AccountController - affiliate" + clientId);

    Header header = Header.builder().clientId(clientId)
        .clientSecret(clientSecret).build();
    if (!accountService.validateCredencial(header))
      throw new AccountForbiddenException("Data" + userAccount);

    Map<String, Object> map = new HashMap<String, Object>();
    UsersAccount usAcc = accountService.affiliate(userAccount);
    map.put("data", usAcc);
    map.put("code", "00000");
    map.put("success", Boolean.TRUE);

    return map;
  }

}
