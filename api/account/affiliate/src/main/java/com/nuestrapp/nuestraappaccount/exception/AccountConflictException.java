package com.nuestrapp.nuestraappaccount.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Class Implements Runtime Exception Conflict.
 *
 * @author Isaias Gabriel Ruza Materano
 * @version 20210129
 * @since 1.0
 * @see com.nuestrapp.nuestraappaccount.exception
 */
@ResponseStatus(HttpStatus.CONFLICT)
public class AccountConflictException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  /**
   * Constructor Account Conflict Exception.
   *
   * @exception Exception Fails Internal Server.
   * @see java.lang.String#charValue()
   */
  public AccountConflictException(String exception) {
    super(exception);
  }

}