package com.nuestrapp.nuestraappaccount.configuration;

import org.springframework.transaction.annotation.EnableTransactionManagement;

@org.springframework.context.annotation.Configuration
@EnableTransactionManagement
public class Configuration {

}
