package com.nuestrapp.nuestraappaccount.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nuestrapp.nuestraappaccount.domain.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

  public List<Account> findByAccountNumber(String accountNumber);
}
