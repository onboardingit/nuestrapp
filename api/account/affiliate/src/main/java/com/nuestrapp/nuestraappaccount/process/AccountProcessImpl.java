package com.nuestrapp.nuestraappaccount.process;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.nuestrapp.nuestraappaccount.NuestraappAccountsApplication;
import com.nuestrapp.nuestraappaccount.domain.Account;
import com.nuestrapp.nuestraappaccount.domain.Header;
import com.nuestrapp.nuestraappaccount.domain.User;
import com.nuestrapp.nuestraappaccount.domain.UsersAccount;
import com.nuestrapp.nuestraappaccount.exception.AccountConflictException;
import com.nuestrapp.nuestraappaccount.exception.AccountNotFoundException;
import com.nuestrapp.nuestraappaccount.persistence.AccountRepository;
import com.nuestrapp.nuestraappaccount.persistence.UserRepository;
import com.nuestrapp.nuestraappaccount.persistence.UsersAccountRepository;

/**
 * Class Implements Process for services accounts.
 *
 * @author Isaias Gabriel Ruza Materano
 * @version 20210129
 * @since 1.0
 * @see com.nuestrapp.nuestraappaccount.service
 */
@Component
@Transactional("transactionManager")
public class AccountProcessImpl implements AccountProcess {

  private static final Logger logger = LogManager
      .getLogger(NuestraappAccountsApplication.class);

  @Autowired
  private AccountRepository accountRepository;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private UsersAccountRepository usersAccountRepository;
//
//  @Autowired
//  private UserAccountRepository userAccountRepository;

  /**
   * Validate Token.
   *
   * @return Objet.
   * @exception Exception Fails Internal Server.
   * @see java.lang.Character#charValue()
   */
  @Override
  public Boolean validateToken(Header header) throws Exception {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * Validate Credencials Header.
   *
   * @return Objet.
   * @exception Exception Fails Internal Server.
   * @see java.lang.Character#charValue()
   */
  @Override
  public Boolean validateCredencial(Header header) throws Exception {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * Affiliate user and account.
   *
   * @return Objeto de Account.
   * @exception Exception Fails Internal Server.
   * @see java.lang.Character#charValue()
   */
  @Override
  public UsersAccount affiliate(UsersAccount userAccount) throws Exception {

    // TODO Auto-generated method stub

    List<Account> acc = accountRepository
        .findByAccountNumber(userAccount.getAccount().getAccountNumber());

    if (acc.size() != 0)
      throw new AccountConflictException("Repeat Account: " + userAccount);

    List<User> uss = userRepository
        .findByIdentityId(userAccount.getUser().getIdentityId());

    if (uss.size() != 0)
      throw new AccountConflictException("Repeat User: " + userAccount);

    // Esto se puede hacer personalizado en uno solo

    uss = userRepository.findByEmail(userAccount.getUser().getEmail());

    if (uss.size() != 0)
      throw new AccountConflictException("Repeat Email: " + userAccount);

    Account account = accountRepository.save(userAccount.getAccount());
    if (account.getId() == null)
      throw new AccountConflictException(
          "Fails Register Account: " + userAccount);

    User user = userRepository.save(userAccount.getUser());

    if (user.getId() == null)
      throw new AccountConflictException("Fails Register User: " + userAccount);

    UsersAccount ua = UsersAccount.builder().user(user).account(account)
        .build();

    UsersAccount usAcc = usersAccountRepository.save(ua);
    if (usAcc.getId() == null)
      throw new AccountConflictException("Fails Register: " + userAccount);

    return null;
  }

}
