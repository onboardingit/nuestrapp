package com.nuestrapp.nuestraappaccount.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import com.nuestrapp.nuestraappaccount.service.AccountService;
import com.nuestrapp.nuestraappaccount.domain.Header;

/**
 * Class Implements Handler Interceptor.
 *
 * @author Isaias Gabriel Ruza Materano
 * @version 20210129
 * @since 1.0
 * @see com.nuestrapp.nuestraappaccount.interceptor
 */
@Component
public class AccountServiceInterceptor implements HandlerInterceptor {

  @Autowired
  private AccountService accountService;

  /**
   * Pre Handle.
   *
   * @return Boolean.
   * @exception Exception Fails Internal Server.
   * @see java.lang.String#Value()
   */
  @Override
  public boolean preHandle(HttpServletRequest request,
      HttpServletResponse response, Object handler) throws Exception {

    Header header = Header.builder()
        .authorization(request.getHeader("Authorization")).build();
    boolean boo = true;

//    if (request.getRequestURI().equals("/api/accounts/affiliate")
//        || request.getRequestURI()
//            .equals("/api/accounts/affiliate/health-check")
//        || request.getRequestURI().equals("/swagger-ui.html")
//        || request.getRequestURI().equals("/swagger-ui.html#/account45controller")
//        || request.getRequestURI().equals("/swagger-ui.html#!/account45controller/affiliateUsingPOST")) {
//      boo = true;
//    } else {
//      boo = accountService.validateToken(header);
//    }

    return boo;
  }

  /**
   * Post Handle.
   *
   * @return method not implement.
   * @exception Exception Fails Internal Server.
   * @see java.lang.String#Value()
   */
  @Override
  public void postHandle(HttpServletRequest request,
      HttpServletResponse response, Object handler, ModelAndView modelAndView)
      throws Exception {

  }

  /**
   * Post Handle.
   *
   * @return method not implement.
   * @exception Exception Fails Internal Server.
   * @see java.lang.String#Value()
   */
  @Override
  public void afterCompletion(HttpServletRequest request,
      HttpServletResponse response, Object handler, Exception exception)
      throws Exception {

  }
}
