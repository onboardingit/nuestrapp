package com.nuestrapp.nuestraappaccount.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nuestrapp.nuestraappaccount.domain.UsersAccount;

public interface UsersAccountRepository
    extends JpaRepository<UsersAccount, Long> {

}
