package com.nuestrapp.nuestraappaccount.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nuestrapp.nuestraappaccount.domain.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

  public List<User> findByIdentityId(String identityId);
  
  public List<User> findByEmail(String email);
}
