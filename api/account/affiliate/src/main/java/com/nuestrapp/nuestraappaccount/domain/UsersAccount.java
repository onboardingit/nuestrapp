package com.nuestrapp.nuestraappaccount.domain;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
@javax.persistence.Entity
@Table(name = "users_accounts")
public class UsersAccount {

  /**
   * Id APP Columna: yappi_status.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "idSeq")
  @SequenceGenerator(name = "idSeq", sequenceName = "users_accounts_id_seq", allocationSize = 1)
  private Integer id;

  /**
   * Creation Date APP Columna: yappi_status.
   */
  @Column(name = "creation_date")
  private Timestamp creationDate;
  /**
   * Status dentro de la Aplicacion Columna: yappi_status.
   */
  @Column(name = "yappy_status")
  private String yappiStatus;

  /**
   * Limite de transacion Columna: transaction_limit.
   */
  @Column(name = "transaction_limit")
  private Double transactionLimit;

  /**
   * Entidad de Negocio de User Columna: user.
   */
   	//bi-directional many-to-one association to Account
	@ManyToOne
  private User user;

  /**
   * Entidad de Negocio de lista de Account Columna: account.
   */
	//bi-directional many-to-one association to Account
	@ManyToOne
  private Account account;

}
