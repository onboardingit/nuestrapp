package com.nuestrapp.nuestraappaccount.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Class Implements Runtime Exception Not Found.
 *
 * @author Isaias Gabriel Ruza Materano
 * @version 20210129
 * @since 1.0
 * @see com.nuestrapp.nuestraappaccount.exception
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class AccountNotFoundException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  /**
   * Constructor Account Not Found Exception.
   *
   * @exception Exception Fails Internal Server.
   * @see java.lang.String#charValue()
   */
  public AccountNotFoundException(String exception) {
    super(exception);
  }

}