package com.nuestrapp.nuestraappaccount.service;

import com.nuestrapp.nuestraappaccount.domain.Header;
import com.nuestrapp.nuestraappaccount.domain.UsersAccount;

/**
 * Interface Service for services accounts.
 *
 * @author Isaias Gabriel Ruza Materano
 * @version 20210129
 * @since 1.0
 * @see com.nuestrapp.nuestraappaccount.service
 */
public interface AccountService {

  /**
   * Validate Credencials Header.
   *
   * @return Objet.
   * @exception Exception Fails Internal Server.
   * @see java.lang.Character#charValue()
   */
  public Boolean validateCredencial(Header header) throws Exception;

  /**
   * Validate Token.
   *
   * @return Objet.
   * @exception Exception Fails Internal Server.
   * @see java.lang.Character#charValue()
   */
  public Boolean validateToken(Header header) throws Exception;

  /**
   * Affiliate user and account.
   *
   * @return Objeto de Account.
   * @exception Exception Fails Internal Server.
   * @see java.lang.Character#charValue()
   */
  public UsersAccount affiliate(UsersAccount userAccount) throws Exception;

}
