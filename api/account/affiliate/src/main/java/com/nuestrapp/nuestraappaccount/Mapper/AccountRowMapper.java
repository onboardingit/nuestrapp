package com.nuestrapp.nuestraappaccount.Mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.nuestrapp.nuestraappaccount.domain.Account;

public class AccountRowMapper implements RowMapper<Account> {

  @Override
  public Account mapRow(ResultSet rs, int arg1) throws SQLException {

    Account acc = Account.builder().id(rs.getLong("id"))
        .accountNumber(rs.getString("account_number"))
        .accountStatus(rs.getString("account_status"))
        .balance(rs.getDouble("balance"))
        .accountType(rs.getString("account_type")).build();

    return acc;
  }
}
