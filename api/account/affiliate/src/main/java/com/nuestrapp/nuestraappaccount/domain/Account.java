package com.nuestrapp.nuestraappaccount.domain;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

/**
 * Objeto de negocios que modela un archivo. Tabla relacionada: Accounts
 *
 * @author iruza
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@javax.persistence.Entity
@Table(name = "account")
public class Account {

  /**
   * Id Cuenta Cliente. Columna: account_number
   */
  @NotNull
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "idSeq")
  @SequenceGenerator(name = "idSeq", sequenceName = "account_id_seq", allocationSize = 1)
  private Long id;

  /**
   * Cuenta Cliente Columna: account_number.
   */
  @NotNull
  @Column(name = "account_number")
  private String accountNumber;

  /**
   * Status de activacion Columna: account_status.
   */
  @NotNull
  @Column(name = "account_status")
  private String accountStatus;

  /**
   * Status de activacion Columna: balance.
   */
  @NotNull
  @Column(name = "balance")
  private Double balance;

  /**
   * Id de usuario Columna: account_type.
   */
  @NotNull
  @Column(name = "account_type")
  private String accountType;

  
  
}
