package com.nuestrapp.nuestraappaccount.domain;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Objeto de negocios que modela un archivo Tabla relacionada: Accounts.
 *
 * @author iruza
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@javax.persistence.Entity
@Table(name = "customer")
public class User {

  /**
   * Id del cliente Columna: id.
   */
  @NotNull
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "idSeq")
  @SequenceGenerator(name = "idSeq", sequenceName = "customer_id_seq", allocationSize = 1)
  private Long id;

  /**
   * Nombre Cliente Columna: firts_name.
   */
  @Column(name = "first_name")
  private String firstName;

  /**
   * Apellido Cliente Columna: last_name.
   */
  @Column(name = "last_name")
  private String lastName;

  /**
   * Identificacion del cliente Columna: identity_id.
   */
  @NotEmpty
  @NotNull
  @Column(name = "identity_id")
  private String identityId;

  /**
   * Tipo de Identificacion Columna: identity_type.
   */
  @NotEmpty
  @NotNull
  @Column(name = "identity_type")
  private String identityType;

  /**
   * Celular Columna: phone_number.
   */
  @NotEmpty
  @NotNull
  @Column(name = "phone_number")
  private String phoneNumber;

  /**
   * Email Columna: email.
   */
  @NotEmpty
  @Email
  @Column(name = "email")
  private String email;

  /**
   * Password Columna: password.
   */
  @NotEmpty
  @Column(name = "password")
  private String password;
}
