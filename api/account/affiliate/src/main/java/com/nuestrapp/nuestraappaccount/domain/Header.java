package com.nuestrapp.nuestraappaccount.domain;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class Header {
  /**
   * Id del api Columna: id.
   */
  @NotNull
  private Long id;

  /**
   * Client Id de la transacción Columna: client_id.
   */
  private String clientId;

  /**
   * Client Secret de la transacción Columna: last_name.
   */
  private String clientSecret;

  /**
   * Authorization Columna: token.
   */
  private String authorization;
}
