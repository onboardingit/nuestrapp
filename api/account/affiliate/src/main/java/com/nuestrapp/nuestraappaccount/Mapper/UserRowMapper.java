package com.nuestrapp.nuestraappaccount.Mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.nuestrapp.nuestraappaccount.domain.User;

public class UserRowMapper implements RowMapper<User> {

  @Override
  public User mapRow(ResultSet rs, int arg1) throws SQLException {

    User user = User.builder().id(rs.getLong("id"))
        .firstName(rs.getString("first_name"))
        .lastName(rs.getString("last_name"))
        .identityId(rs.getString("identity_id"))
        .identityType(rs.getString("identity_type"))
        .phoneNumber(rs.getString("phone_number")).email(rs.getString("email"))
        .build();

    return user;
  }
}
