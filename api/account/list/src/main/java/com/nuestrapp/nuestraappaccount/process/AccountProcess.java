package com.nuestrapp.nuestraappaccount.process;

import java.util.List;

import com.nuestrapp.nuestraappaccount.domain.Account;
import com.nuestrapp.nuestraappaccount.domain.Header;
import com.nuestrapp.nuestraappaccount.domain.User;
import com.nuestrapp.nuestraappaccount.domain.UserAccount;

public interface AccountProcess {

  public List<Account> findAllById(UserAccount userAccount);

  public Boolean validateToken(Header header) throws Exception;

}
