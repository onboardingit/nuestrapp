package com.nuestrapp.nuestraappaccount.domain;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
public class UserAccount {

	/**
	 * Status dentro de la Aplicacion
	 * 
	 * Columna: yappi_status
	 */
	private String yappiStatus;

	/**
	 * Limite de transacion
	 * 
	 * Columna: transaction_limit
	 */
	private Double transactionLimit;

	/**
	 * Entidad de Negocio de User
	 * 
	 * Columna: user
	 */
	private User user;

	/**
	 * Entidad de Negocio de lista de Account
	 * 
	 * Columna: account
	 */
	private Account account;

}
