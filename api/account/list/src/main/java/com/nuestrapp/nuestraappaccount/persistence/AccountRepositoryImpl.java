package com.nuestrapp.nuestraappaccount.persistence;

import java.util.HashMap;
import java.util.List;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.nuestrapp.nuestraappaccount.NuestraappAccountsApplication;
import com.nuestrapp.nuestraappaccount.Mapper.AccountRowMapper;
import com.nuestrapp.nuestraappaccount.Mapper.UserAccountRowMapper;
import com.nuestrapp.nuestraappaccount.Mapper.UserRowMapper;
import com.nuestrapp.nuestraappaccount.domain.Account;
import com.nuestrapp.nuestraappaccount.domain.User;
import com.nuestrapp.nuestraappaccount.domain.UserAccount;

@Repository
public class AccountRepositoryImpl implements AccountRepository {

  private static final Logger logger = LogManager
      .getLogger(NuestraappAccountsApplication.class);

  @Value("${app.nuestraapp.schema}")
  private String SCHEMA_BD;

  public AccountRepositoryImpl(NamedParameterJdbcTemplate template) {
    this.template = template;
  }

  NamedParameterJdbcTemplate template;

  @Override
  public List<Account> findAllById(UserAccount userAccount) {
    return template.query("select a.* from " + SCHEMA_BD + "customer u, "
        + SCHEMA_BD + "account a, " + SCHEMA_BD
        + "users_accounts ua where u.id = ua.user_id and a.id = ua.account_id and ua.user_id = (select id from "
        + SCHEMA_BD + "customer where identity_id = '"
        + userAccount.getUser().getIdentityId() + "')", new AccountRowMapper());
  }

}
