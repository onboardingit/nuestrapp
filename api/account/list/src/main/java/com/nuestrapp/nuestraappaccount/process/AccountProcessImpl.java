package com.nuestrapp.nuestraappaccount.process;

import java.util.List;

import com.nuestrapp.nuestraappaccount.exception.AccountNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nuestrapp.nuestraappaccount.NuestraappAccountsApplication;
import com.nuestrapp.nuestraappaccount.domain.Account;
import com.nuestrapp.nuestraappaccount.domain.Header;
import com.nuestrapp.nuestraappaccount.domain.UserAccount;
import com.nuestrapp.nuestraappaccount.persistence.AccountRepository;

@Component
public class AccountProcessImpl implements AccountProcess {

  private static final Logger logger = LogManager
      .getLogger(NuestraappAccountsApplication.class);

  @Autowired
  private AccountRepository accountRepository;

  @Override
  public List<Account> findAllById(UserAccount userAccount) {

    List<Account> accounts = accountRepository.findAllById(userAccount);

    if (accounts.size() == 0)
      throw new AccountNotFoundException("NO ACCOUNTS FOUND");

    return accounts;
  }

  @Override
  public Boolean validateToken(Header header) throws Exception {
    // TODO Auto-generated method stub
    return null;
  }

}
