package com.nuestrapp.nuestraappaccount.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nuestrapp.nuestraappaccount.NuestraappAccountsApplication;
import com.nuestrapp.nuestraappaccount.domain.Account;
import com.nuestrapp.nuestraappaccount.domain.Header;
import com.nuestrapp.nuestraappaccount.domain.UserAccount;
import com.nuestrapp.nuestraappaccount.exception.AccountForbiddenException;
import com.nuestrapp.nuestraappaccount.service.AccountService;

@RestController
@RequestMapping("/api/accounts")
public class AccountController {

  private static final Logger logger = LogManager
      .getLogger(NuestraappAccountsApplication.class);

  @Autowired
  private AccountService accountService;

  @CrossOrigin
  @PostMapping("list")
  public Map<String, Object> findAllById(@RequestBody UserAccount userAccount) {

    logger.debug("Start AccountController ");

    List<Account> accounts = accountService.findAllById(userAccount);
    Map<String, Object> map = this.response(accounts, Boolean.TRUE);

    return map;
  }

  public Map<String, Object> response(List<Account> accounts, Boolean resp) {

    Map<String, Object> map = new HashMap<String, Object>();
    map.put("data", accounts);
    map.put("totalAccount", accounts.size());
    map.put("success", resp);

    return map;
  }

}
