package com.nuestrapp.nuestraappaccount.service;

import java.util.List;

import com.nuestrapp.nuestraappaccount.domain.Account;
import com.nuestrapp.nuestraappaccount.domain.Header;
import com.nuestrapp.nuestraappaccount.domain.UserAccount;

public interface AccountService {

  public List<Account> findAllById(UserAccount userAccount);

  public Boolean validateCredencial(Header header) throws Exception;

  public Boolean validateToken(Header header) throws Exception;

}
