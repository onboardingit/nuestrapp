package com.nuestrapp.nuestraappaccount.Mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.nuestrapp.nuestraappaccount.domain.Account;
import com.nuestrapp.nuestraappaccount.domain.User;
import com.nuestrapp.nuestraappaccount.domain.UserAccount;

public class UserAccountRowMapper implements RowMapper<UserAccount> {

	@Override
	public UserAccount mapRow(ResultSet rs, int arg1) throws SQLException {
		
		
		Account acc = Account.builder().id(rs.getLong("id_account")).accountNumber(rs.getString("account_number"))
				.accountStatus(rs.getString("account_status")).balance(rs.getDouble("balance"))
				.accountType(rs.getString("account_type")).build();
		
		
		User user = User.builder().id(rs.getLong("id_user")).firstName(rs.getString("first_name"))
				.lastName(rs.getString("last_name")).identityId(rs.getString("identity_id"))
				.identityType(rs.getString("identity_type")).phoneNumber(rs.getString("phone_number"))
				.email(rs.getString("email")).build();
		
		UserAccount usAcc = UserAccount.builder().user(user).account(acc).transactionLimit(rs.getDouble("transaction_limit")).build();

		return usAcc;
	}
}
