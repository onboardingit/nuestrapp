package com.nuestrapp.nuestraappaccount.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class Header {
	/**
	 * Id del api
	 * 
	 * Columna: id
	 */
	@NotNull
	private Long id;

	/**
	 * Client Id de la transacción
	 * 
	 * Columna: client_id
	 */
	@Size(min=2, max=50)
	private String clientId;

	/**
	 * Client Secret de la transacción
	 * 
	 * Columna: last_name
	 */
	@Size(min=2, max=50)
	private String clientSecret;
	
	/**
	 * Authorization
	 * 
	 * Columna: token
	 */
	private String authorization;
}
