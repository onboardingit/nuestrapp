package com.nuestrapp.nuestraappaccount.domain;

import javax.validation.constraints.NotNull;

import lombok.*;

/**
 * Objeto de negocios que modela un archivo
 * 
 * Tabla relacionada: Accounts
 * 
 * @author iruza
 * 
 */

@Data
@Builder
@AllArgsConstructor
public class Account {

	/**
	 * Id Cuenta Cliente
	 * 
	 * Columna: account_number
	 */
	@NotNull
	private Long id;

	/**
	 * Cuenta Cliente
	 * 
	 * Columna: account_number
	 */
	@NotNull
	private String accountNumber;

	/**
	 * Status de activacion
	 * 
	 * Columna: account_status
	 */
	@NotNull
	private String accountStatus;

	/**
	 * Status de activacion
	 * 
	 * Columna: balance
	 */
	@NotNull
	private Double balance;

	/**
	 * Id de usuario
	 * 
	 * Columna: account_type
	 */
	@NotNull
	private String accountType;

}
