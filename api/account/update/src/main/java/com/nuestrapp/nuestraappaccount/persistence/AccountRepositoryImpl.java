package com.nuestrapp.nuestraappaccount.persistence;

import java.util.HashMap;
import java.util.List;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.nuestrapp.nuestraappaccount.NuestraappAccountsApplication;
import com.nuestrapp.nuestraappaccount.Mapper.AccountRowMapper;
import com.nuestrapp.nuestraappaccount.Mapper.UserAccountRowMapper;
import com.nuestrapp.nuestraappaccount.Mapper.UserRowMapper;
import com.nuestrapp.nuestraappaccount.domain.Account;
import com.nuestrapp.nuestraappaccount.domain.User;
import com.nuestrapp.nuestraappaccount.domain.UserAccount;

@Repository
public class AccountRepositoryImpl implements AccountRepository {

  private static final Logger logger = LogManager
      .getLogger(NuestraappAccountsApplication.class);

  @Value("${app.nuestraapp.schema}")
  private String SCHEMA_BD;

  public AccountRepositoryImpl(NamedParameterJdbcTemplate template) {
    this.template = template;
  }

  NamedParameterJdbcTemplate template;

  @Override
  public boolean updateAccount(Account acc) {

    Map<String, String> paramMap = new HashMap<String, String>();
    paramMap.put("balance", acc.getBalance().toString());
    paramMap.put("account_number", acc.getAccountNumber());
    return template.update("update " + SCHEMA_BD
        + "account set balance = :balance where account_number = :account_number",
        paramMap) > 0;
  }

}
