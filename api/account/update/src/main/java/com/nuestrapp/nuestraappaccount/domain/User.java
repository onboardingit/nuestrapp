package com.nuestrapp.nuestraappaccount.domain;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.*;

/**
 * Objeto de negocios que modela un archivo
 * 
 * Tabla relacionada: Accounts
 * 
 * @author iruza
 * 
 */

@Data
@Builder
@AllArgsConstructor
public class User {

	/**
	 * Id del cliente
	 * 
	 * Columna: id
	 */
	@NotNull
	private Long id;

	/**
	 * Nombre Cliente
	 * 
	 * Columna: firts_name
	 */
	@Size(min=2, max=30)
	private String firstName;

	/**
	 * Apellido Cliente
	 * 
	 * Columna: last_name
	 */
	@Size(min=2, max=30)
	private String lastName;
		
	/**
	 * Identificacion del cliente
	 * 
	 * Columna: identity_id
	 */
	@NotEmpty @NotNull
	private String identityId;
	
	/**
	 * Tipo de Identificacion
	 * 
	 * Columna: identity_type
	 */
	@NotEmpty @NotNull
	private String identityType;	
	
	/**
	 * Celular
	 * 
	 * Columna: phone_number
	 */
	@NotEmpty @NotNull
	private String phoneNumber;
	
	/**
	 * Email
	 * 
	 * Columna: email
	 */
	@NotEmpty @Email
	private String email;
	
	/**
	 * Password
	 * 
	 * Columna: password
	 */
	@NotEmpty
	private String password;
}
