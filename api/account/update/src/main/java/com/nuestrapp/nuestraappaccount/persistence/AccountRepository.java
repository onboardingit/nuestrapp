package com.nuestrapp.nuestraappaccount.persistence;

import com.nuestrapp.nuestraappaccount.domain.Account;

public interface AccountRepository {

  public boolean updateAccount(Account acc);

}
