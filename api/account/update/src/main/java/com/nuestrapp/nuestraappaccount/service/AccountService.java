package com.nuestrapp.nuestraappaccount.service;

import java.util.List;

import com.nuestrapp.nuestraappaccount.domain.Header;
import com.nuestrapp.nuestraappaccount.domain.UserAccount;

public interface AccountService {

  public boolean updateAccount(UserAccount userAccount) throws Exception;

  public Boolean validateCredencial(Header header) throws Exception;

  public Boolean validateToken(Header header) throws Exception;

}
