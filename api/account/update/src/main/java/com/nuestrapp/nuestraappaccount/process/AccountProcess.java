package com.nuestrapp.nuestraappaccount.process;

import java.util.List;

import com.nuestrapp.nuestraappaccount.domain.Account;
import com.nuestrapp.nuestraappaccount.domain.Header;
import com.nuestrapp.nuestraappaccount.domain.User;
import com.nuestrapp.nuestraappaccount.domain.UserAccount;

public interface AccountProcess {

  public boolean updateAccount(Account acc) throws Exception;

  public Boolean validateToken(Header header) throws Exception;

}
