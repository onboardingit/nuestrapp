package com.nuestrapp.nuestraappaccount.process;

import java.util.List;

import com.nuestrapp.nuestraappaccount.exception.AccountNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nuestrapp.nuestraappaccount.NuestraappAccountsApplication;
import com.nuestrapp.nuestraappaccount.domain.Account;
import com.nuestrapp.nuestraappaccount.domain.Header;
import com.nuestrapp.nuestraappaccount.domain.User;
import com.nuestrapp.nuestraappaccount.domain.UserAccount;
import com.nuestrapp.nuestraappaccount.exception.AccountConflictException;
import com.nuestrapp.nuestraappaccount.persistence.AccountRepository;

@Component
public class AccountProcessImpl implements AccountProcess {

  private static final Logger logger = LogManager
      .getLogger(NuestraappAccountsApplication.class);

  @Autowired
  private AccountRepository accountRepository;

  @Override
  public boolean updateAccount(Account acc) throws Exception {
    // TODO Auto-generated method stub
    return accountRepository.updateAccount(acc);
  }

  @Override
  public Boolean validateToken(Header header) throws Exception {
    // TODO Auto-generated method stub
    return null;
  }

}
