package com.nuestrapp.nuestraappaccount;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NuestraappAccountsApplication {

	public static void main(String[] args) {
		SpringApplication.run(NuestraappAccountsApplication.class, args);
	}

}
