package com.nuestrapp.nuestraappaccount.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.nuestrapp.nuestraappaccount.domain.Header;
import com.nuestrapp.nuestraappaccount.service.AccountService;

@Component
public class AccountServiceInterceptor implements HandlerInterceptor {

	@Autowired
	private AccountService accountService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		Header header = Header.builder().authorization(request.getHeader("Authorization")).build();
		boolean boo = true;

		if (request.getRequestURI().equals("/api/accounts/affiliate")
				|| request.getRequestURI().equals("/api/accounts/find/health-check")) {
			boo = true;
		} else {
			boo = accountService.validateToken(header);
		}

		return boo;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception exception) throws Exception {

	}
}
