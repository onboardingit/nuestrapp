package com.nuestrapp.nuestraappaccount.process;

import java.util.List;

import com.nuestrapp.nuestraappaccount.domain.Account;
import com.nuestrapp.nuestraappaccount.domain.Header;
import com.nuestrapp.nuestraappaccount.domain.User;
import com.nuestrapp.nuestraappaccount.domain.UserAccount;

public interface AccountProcess {

	public List<Account> findAccountById(UserAccount userAccount);

}
