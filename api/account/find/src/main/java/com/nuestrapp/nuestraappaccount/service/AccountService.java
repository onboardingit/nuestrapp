package com.nuestrapp.nuestraappaccount.service;

import java.util.List;

import com.nuestrapp.nuestraappaccount.domain.Account;
import com.nuestrapp.nuestraappaccount.domain.Header;
import com.nuestrapp.nuestraappaccount.domain.UserAccount;

public interface AccountService {

	public Boolean validateToken(Header header) throws Exception;

	public List<Account> findAccountById(UserAccount userAccount);

}
