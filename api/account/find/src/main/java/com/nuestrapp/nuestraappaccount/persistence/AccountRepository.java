package com.nuestrapp.nuestraappaccount.persistence;

import java.util.List;

import com.nuestrapp.nuestraappaccount.domain.Account;
import com.nuestrapp.nuestraappaccount.domain.UserAccount;

public interface AccountRepository {

  public List<Account> findAccountById(UserAccount userAccount);

}
