package com.nuestrapp.nuestraappaccount.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nuestrapp.nuestraappaccount.NuestraappAccountsApplication;
import com.nuestrapp.nuestraappaccount.domain.Account;
import com.nuestrapp.nuestraappaccount.domain.UserAccount;
import com.nuestrapp.nuestraappaccount.exception.AccountBadRequestException;
import com.nuestrapp.nuestraappaccount.domain.Header;
import com.nuestrapp.nuestraappaccount.process.AccountProcess;

@Service
@Transactional("transactionManager")
public class AccountServiceImpl implements AccountService {

	private static final Logger logger = LogManager.getLogger(NuestraappAccountsApplication.class);

	@Autowired
	private AccountProcess accountProcess;


	@Override
	public Boolean validateToken(Header header) throws Exception {
		// TODO Auto-generated method stub

		if (header.getAuthorization() == null || header.getAuthorization().isEmpty())
			throw new AccountBadRequestException("Write token");

		return true;
	}

	@Override
	public List<Account> findAccountById(UserAccount userAccount) {
		// TODO Auto-generated method stub
		if (userAccount.getAccount() == null || userAccount.getAccount().getAccountNumber() == null
				|| userAccount.getAccount().getAccountNumber().isEmpty())
			throw new AccountBadRequestException("Bad Request: " + userAccount);

		return accountProcess.findAccountById(userAccount);
	}

}
