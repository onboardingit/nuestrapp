package com.nuestrapp.nuestraappaccount.legacy;

import java.util.List;

import com.nuestrapp.nuestraappaccount.domain.Account;

public interface AccountLegacy {
	
	public List<Account> findAll();

}
