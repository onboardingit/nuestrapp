package com.nuestra.app.authentication.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nuestra.app.authentication.model.Token;

@Repository
public interface TokenInterface extends JpaRepository<Token, Integer>{
  
  Token findByToken(String Token);

}