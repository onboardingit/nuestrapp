package com.nuestra.app.authentication.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TokenBusinessException extends Exception {

  private static final long serialVersionUID = 1L;

  private long id;
  private String code;
  private HttpStatus httpStatus;

  public TokenBusinessException(long id, String code, String message, HttpStatus httpStatus) {
    super(message);
    this.id = id;
    this.code = code;
    this.httpStatus = httpStatus;
  }

  public TokenBusinessException(String code, String message, HttpStatus httpStatus) {
    super(message);
    this.code = code;
    this.httpStatus = httpStatus;
  }

  public TokenBusinessException(String message, Throwable cause) {
    super(message, cause);
  }
}
