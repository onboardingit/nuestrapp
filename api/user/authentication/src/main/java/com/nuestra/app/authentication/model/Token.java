package com.nuestra.app.authentication.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Entidad que contiene la informacion de un token de acceso de un usuario para el uso de NuestraApp
 * 
 * @author rfeliz
 *
 */
@Data
@Entity
@Table(
    name = "token")
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Token {

  @Id
  @GeneratedValue(
      strategy = GenerationType.AUTO)
  private Long id;

  @Column(
      name = "user_id")
  private Long userId;

  @Column(
      name = "token")
  private String token;

  @Column(
      name = "expiration_date")
  private Date expirationDate;

  @Transient
  private String expirationDateAuth;

  public Token(Long userId, String token, String expirationDate) {
    this.userId = userId;
    this.token = token;
    this.expirationDateAuth = expirationDate;
  }
  
  public Token(Long userId, String token, Date expirationDate) {
    this.userId = userId;
    this.token = token;
    this.expirationDate = expirationDate;
  }
  
  public Token(Long id, Long userId, String token, Date expirationDate) {
    this.id = id;
    this.userId = userId;
    this.token = token;
    this.expirationDate = expirationDate;
  }


}


