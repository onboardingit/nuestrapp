package com.nuestra.app.authentication.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.nuestra.app.authentication.exception.TokenBusinessException;
import com.nuestra.app.authentication.model.Token;
import com.nuestra.app.authentication.model.User;
import com.nuestra.app.authentication.model.UserRequest;
import com.nuestra.app.authentication.repo.TokenInterface;
import com.nuestra.app.authentication.repo.UserInterface;
import com.nuestra.app.authentication.util.JWTUtil;

@Service
public class AuthenticationService {

  @Autowired
  private JWTUtil jwtUril;

  @Autowired
  private TokenInterface tokenInterface;

  @Autowired
  private UserInterface userInterface;

  public Token getToken(UserRequest userRequest) throws TokenBusinessException {
    
    // Genero el token recibiendo por el email del usuario por request
    if (userRequest == null) {
      throw new TokenBusinessException("1030", "Error al recibir el email",
          HttpStatus.BAD_REQUEST);
    }

    // Busco el id del usuario por el email del usuario por request
    User user = getUser(userRequest);

    // Guardo el token
    Token token = saveToken(user);

    return token;
  }

  public Token saveToken(User user) throws TokenBusinessException {

    if (user == null) {
      throw new TokenBusinessException("1040", "Error con los datos del usuario",
          HttpStatus.EXPECTATION_FAILED);
    }

    //Se genera el token con el id del usuario del email
    String stringToken = jwtUril.generateToken(user.getId());
    
    //Se obtiene la fecha de expiracion del token generado
    Date dateExpiration = jwtUril.extractExpiration(stringToken);
    
    //Se crea un nuevo objeto token
    Token newToken = new Token(user.getId(), stringToken, dateExpiration);
    
    //Se guarda el objeto token en bd
    Token tokenSaved = tokenInterface.save(newToken);
    
    //Se crea un token con los datos al devolver
    Token tokenResponse = new Token(user.getId(), tokenSaved.getToken(), String.valueOf(jwtUril.getTIME_MILLIS()));    

    return tokenResponse;
  }

  public User validateToken(Token tokenRequest) throws TokenBusinessException {

    if (tokenRequest == null) {
      throw new IllegalStateException("Atributos faltantes");
    }

    // Se valida si existe el token recibido en el request
    Token tokenSaved = tokenInterface.findByToken(tokenRequest.getToken());

    if (tokenSaved == null) {
      throw new TokenBusinessException("1050", "Error al recibir el token",
          HttpStatus.BAD_REQUEST);
    }

    Boolean isValidToken = jwtUril.validateToken(tokenRequest.getToken());

    if (!isValidToken) {
      throw new TokenBusinessException("1060", "Token invalido",
        HttpStatus.EXPECTATION_FAILED);
    }

    /*Del token del request se obtiene el id del usuario      * 
     * para crear un nuevo token y fecha de expiracion     * 
     * */

    String idUser = jwtUril.extractIdUser(tokenSaved.getToken());

    User user = new User();

    user.setId(Long.valueOf(idUser));
    
    return user;

  }

  public Token getRefresh(Token tokenRequest) throws TokenBusinessException {

    if (tokenRequest == null) {
      throw new IllegalStateException("Atributos faltantes");
    }

    // Se valida si existe el token recibido en el request
    Token token = tokenInterface.findByToken(tokenRequest.getToken());

    if (token == null) {
      throw new TokenBusinessException("1070", "Error al recibir el token",
          HttpStatus.BAD_REQUEST);
    }

    Boolean isValidToken = jwtUril.validateToken(tokenRequest.getToken());
    if (!isValidToken) {
      throw new TokenBusinessException("1060", "Token invalido",
          HttpStatus.EXPECTATION_FAILED);
    }

    // Del token del request se obtiene el id del usuario para crear un nuevo token y fecha de
    // expiracion

    //Obtengo el id del usuario del token recibido por request
    String idUser = jwtUril.extractIdUser(token.getToken());
    
    //Se genera un nuevo token
    String newToken = jwtUril.generateToken(Long.valueOf(idUser));
    
    //Se obtiene la fecha de expiracion del nuevo token
    Date newDate = jwtUril.extractExpiration(newToken);

    //Se un objecto token con los datos del nuevo token
    Token refreshedToken =
        new Token(token.getId(), Long.valueOf(idUser), newToken, newDate);

    //Se atualiza en bd
    Token tokenUpdated = tokenInterface.save(refreshedToken);
    
    //Se crea otro token con los datos al devolver en el response
    Token tokenResponse = 
        new Token(tokenUpdated.getUserId(), tokenUpdated.getToken(), String.valueOf(jwtUril.getTIME_MILLIS()));

    return tokenResponse;

  }

  public void delete(Token tokenRequest) throws TokenBusinessException {
    // Verificar si de puede retonar un respuesta de eliminado

    // Se valida si existe el token recibido en el request
    Token tokenSaved = tokenInterface.findByToken(tokenRequest.getToken());

    if (tokenSaved == null) {
      throw new TokenBusinessException("1030", "Error al recibir el token",
          HttpStatus.BAD_REQUEST);
    }

    // Se elimina el token del usuario por el token recibido del request
    tokenInterface.delete(tokenRequest);

  }

  private User getUser(UserRequest userRequest) throws TokenBusinessException {

    if (userRequest == null) {
      throw new TokenBusinessException("1030", "Error al recibir el email",
          HttpStatus.BAD_REQUEST);
    }

    // Se buscar el usuario por email
    User user = userInterface.findByEmail(userRequest.getEmail());

    return user;
  }

}
