package com.nuestra.app.authentication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nuestra.app.authentication.exception.TokenBusinessException;
import com.nuestra.app.authentication.model.Token;
import com.nuestra.app.authentication.model.User;
import com.nuestra.app.authentication.model.UserRequest;
import com.nuestra.app.authentication.service.AuthenticationService;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@RestController
@EnableSwagger2
@RequestMapping("/authentication")
public class AuthenticationRestController {

  @Autowired
  AuthenticationService authenticationService;

  @GetMapping("/health-check")
  public String healthCheck() {

    return "Hello World from Authentication";
  }

  @PostMapping("/token")
  public ResponseEntity<Token> token(@RequestBody UserRequest userRequest) throws TokenBusinessException {

    //Se genera el token
    Token token = authenticationService.getToken(userRequest);

    return ResponseEntity.ok(token);

  }

  @PostMapping("/refresh")
  public ResponseEntity<?> refresh(@RequestBody Token tokenRequest) throws TokenBusinessException {

    //Se actualiza el token
    Token token = authenticationService.getRefresh(tokenRequest);

    return ResponseEntity.ok(token);

  }


  @PostMapping("/validate")
  public ResponseEntity<User> validate(@RequestBody Token tokenRequest) throws TokenBusinessException {
    
    //Se valida si ha expirado el token
    User response = authenticationService.validateToken(tokenRequest);

    return ResponseEntity.ok(response);

  }

  @PostMapping("/logout")
  public ResponseEntity<?> logout(@RequestBody Token tokenRequest) throws TokenBusinessException {

    //Se elimina el token

    authenticationService.delete(tokenRequest);

    return ResponseEntity.accepted().build();

  }

}
