package com.nuestra.app.authentication.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nuestra.app.authentication.model.User;

@Repository
public interface UserInterface extends JpaRepository<User, Integer>{
  
  User findByEmail(String email);

}
