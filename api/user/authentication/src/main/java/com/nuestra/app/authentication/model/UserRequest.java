package com.nuestra.app.authentication.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Valores necesarios en la solicitud de un token con informacion del usuario.
 * 
 * @author rfeliz
 *
 */
@Getter
@Setter
public class UserRequest {

  /* Email del usuario de NuestraApp. */
  private String email;

}