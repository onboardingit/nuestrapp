package com.nuestra.app.signup.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Entida que encierra los datos necesarios de respueta del singUp. Contiene los datos del usuario
 * creado ademas de la informacion del token de acceso.
 * 
 * @author satencio
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {

  /* Contiene la información correspondiente a el usuario */
  private User user;

  /* Contiene los datos un token de acceso de un usario para el uso de NuestraApp */
  private Token token;

}
