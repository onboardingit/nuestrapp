package com.nuestra.app.signup.service;

import com.nuestra.app.signup.exceptions.UserBusinessException;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.nuestra.app.signup.common.EmailValidator;
import com.nuestra.app.signup.model.Token;
import com.nuestra.app.signup.model.User;
import com.nuestra.app.signup.model.UserRepository;
import com.nuestra.app.signup.model.UserResponse;

import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Clase que contiene los servicios con la business logic de la app
 *
 * @author satencio
 */

@Service
@AllArgsConstructor
public class SignUpService {

  @Autowired
  private EmailValidator emailValidator;

  private UserRepository userRepository;

  @Value("${auth.service.host}")
  private String authServiceHost;
  
  @Autowired
  public SignUpService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  public UserResponse signUp(User user) throws UserBusinessException {

    boolean isValidEmail;

    if (user == null) {
      throw new UserBusinessException("1024", "Objeto null", HttpStatus.FORBIDDEN);
    }

    isValidEmail = emailValidator.test(user.getEmail());

    if (!isValidEmail) {
      throw new UserBusinessException("1024", "Formato de Email invalido", HttpStatus.BAD_REQUEST);
    }

    Optional<User> userEmail = userRepository
            .findByEmail(user.getEmail());

    if(userEmail.isPresent()){
      throw new UserBusinessException("1024", "Email ya existente", HttpStatus.BAD_REQUEST);
    }

    User userSaved = userRepository.save(user);

    Token tokenData = getToken(userSaved);

    userSaved.setPassword(null);

    userSaved.setId(null);

    UserResponse response = new UserResponse(userSaved, tokenData);

    return response;
  }


  private Token getToken(User user) {

    WebClient webClient = WebClient.create(authServiceHost);

    Token token = webClient.post().uri("/token").body(Mono.just(user), User.class).retrieve()
        .bodyToMono(Token.class).block();

    return token;
  }

}
