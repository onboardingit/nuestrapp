package com.nuestra.app.signup.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Entidad que representa el body que recibe el POST request del endpoint /sign-up
 *
 * @author satencio
 */
@Data
@Entity
@Table(
    name = "customer")
@NoArgsConstructor
@AllArgsConstructor
public class User {

  @Id
  @GeneratedValue(
      strategy = GenerationType.AUTO)
  private Long id;

  /* Contiene el nombre del usuario */
  private String firstName;

  /* Contiene el apellido del usuario */
  private String lastName;

  /* Contiene la identificación personal única del usuario */
  private String identityId;

  /* Contiene el tipo de identificación del usuario del usuario ejm: cédula, pasaporte, licencia */
  private String identityType;

  /* Contiene el número telefonico vinculado del usuario */
  private String phoneNumber;

  /* Contiene el correo electrónico vinculado del usuario */
  private String email;

  /* Contiene la password encriptada del usuario */
  private String password;

  public User(String firstName, String lastName, String identityId, String identityType,
      String phoneNumber, String email, String password) {

    this.firstName = firstName;
    this.lastName = lastName;
    this.identityId = identityId;
    this.identityType = identityType;
    this.phoneNumber = phoneNumber;
    this.email = email;
    this.password = password;
  }

}
