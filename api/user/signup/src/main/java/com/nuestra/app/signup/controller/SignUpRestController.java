package com.nuestra.app.signup.controller;

import com.nuestra.app.signup.exceptions.UserBusinessException;
import com.nuestra.app.signup.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.nuestra.app.signup.model.UserResponse;
import com.nuestra.app.signup.service.SignUpService;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@RestController
@EnableSwagger2
@RequestMapping(
    path = "/sign-up")
public class SignUpRestController {

  @Autowired
  private SignUpService signUpService;

  @PostMapping
  public ResponseEntity<UserResponse> signUp(@RequestBody User request)
          throws UserBusinessException {

    UserResponse response = signUpService.signUp(request);

    return ResponseEntity.ok(response);
  }

  @GetMapping("/health-check")
  public ResponseEntity<?> token() {

    return new ResponseEntity<String>("Hello! i'm OK", HttpStatus.OK);

  }

}
