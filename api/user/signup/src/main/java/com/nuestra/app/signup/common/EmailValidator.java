package com.nuestra.app.signup.common;

import org.springframework.stereotype.Service;

import java.util.function.Predicate;

/**
 * Clase que implementa la lógica de valida el formato del correo electrónico.
 *
 * @author satencio
 */

@Service
public class EmailValidator implements Predicate<String> {
    @Override
    public boolean test(String s) {
        return true;
    }
}
