package com.nuestra.app.signup.model;

import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Entidad que contiene la informacion de un token de acceso de un usario para el uso de NuestraApp
 * 
 * @author rfliz
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Token {

  /* Valor del token de acceso para un usuario. */
  private String token;

  /* Fecha de expiracion de la autorizacion del token de acceso. */
  private String expirationDate;
  
  @Transient
  private String expirationDateAuth;

}
