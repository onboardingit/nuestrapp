package com.nuestra.app.login.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.nuestra.app.login.exception.UserBusinessException;
import com.nuestra.app.login.model.Customer;
import com.nuestra.app.login.model.CustomerResponse;
import com.nuestra.app.login.model.Token;
import com.nuestra.app.login.repository.CustomerRepository;

import reactor.core.publisher.Mono;

@Service
public class LoginService {

  @Autowired
  CustomerRepository loginRepository;

  @Value("${auth.service.host}")
  private String authServiceHost;

  public CustomerResponse login(Customer credentials) throws UserBusinessException {

    CustomerResponse response = new CustomerResponse();

    Customer userData = getUser(credentials);

    if (userData == null) {
      throw new UserBusinessException("1024", "Usuario No existe", HttpStatus.FORBIDDEN);
    }

    if (!validateCredentials(credentials, userData)) {
      throw new UserBusinessException("1024", "Error de validacion, con el usuario",
          HttpStatus.UNAUTHORIZED);
    }

    Token token = getToken(userData);

    response.setToken(token);
    response.setUser(userData);

    return response;

  }

  private Customer getUser(Customer user) {
    Customer response = loginRepository.findByEmail(user.getEmail());

    return response;
  }

  private boolean validateCredentials(Customer credentials, Customer userData) {

    boolean response = false;

    response = (credentials.getPassword().compareTo(userData.getPassword()) == 0);

    return response;
  }

  private Token getToken(Customer user) {

    WebClient webClient = WebClient.create(authServiceHost);

    Token token = webClient.post().uri("/token").body(Mono.just(user), Customer.class).retrieve()
        .bodyToMono(Token.class).block();

    return token;
  }


}
