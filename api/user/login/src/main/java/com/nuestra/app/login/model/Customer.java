package com.nuestra.app.login.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Customer {

  @Id
  @GeneratedValue(
      strategy = GenerationType.AUTO)
  private Long id;

  /* Contiene el nombre del usuario */
  private String firstName;

  /* Contiene el apellido del usuario */
  private String lastName;

  /* Contiene la identificación personal única del usuario */
  private String identityId;

  /* Contiene el tipo de identificación del usuario del usuario ejm: cédula, pasaporte, licencia */
  private String identityType;

  /* Contiene el número telefonico vinculado del usuario */
  private String phoneNumber;

  /* Contiene el correo electrónico vinculado del usuario */
  private String email;

  /* Contiene la password encriptada del usuario */
  private String password;

  public Customer(String firstName, String lastName, String identityId, String identityType,
      String phoneNumber, String email, String password) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.identityId = identityId;
    this.identityType = identityType;
    this.phoneNumber = phoneNumber;
    this.email = email;
    this.password = password;
  }

}
