package com.nuestra.app.login.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nuestra.app.login.exception.UserBusinessException;
import com.nuestra.app.login.model.Customer;
import com.nuestra.app.login.model.CustomerResponse;
import com.nuestra.app.login.service.LoginService;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@RestController
@EnableSwagger2
@RequestMapping("/login")
public class LoginRestController {

  @Autowired
  private LoginService loginService;

  @GetMapping("/health-check")
  public ResponseEntity<String> healthCheck() {
    return new ResponseEntity<String>("Hello! i'm OK", HttpStatus.OK);
  }

  @PostMapping
  public ResponseEntity<CustomerResponse> login(@RequestBody Customer user)
      throws UserBusinessException {

    CustomerResponse response = loginService.login(user);

    return ResponseEntity.ok(response);

  }

}
