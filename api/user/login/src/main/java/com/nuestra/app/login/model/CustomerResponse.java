package com.nuestra.app.login.model;

import lombok.Data;

/**
 * Entida que encierra los datos necesarios de respueta del singUp. Contiene los datos del usuario
 * creado ademas de la informacion del token de acceso.
 * 
 * @author satencio
 *
 */
@Data
public class CustomerResponse {

  /* Contiene los datos de un usuario de NuestraApp. */
  private Customer user;

  /* Contiene los datos un token de acceso de un usario para el uso de NuestraApp */
  private Token token;

}
