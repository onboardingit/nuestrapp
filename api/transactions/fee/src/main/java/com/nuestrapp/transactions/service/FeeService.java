package com.nuestrapp.transactions.service;

import com.nuestrapp.transactions.domain.Fee;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

@Service
public class FeeService {

    public Optional<Fee> calculateTransactionFee(String accountType, double amount) {
        Fee fee = new Fee();

        if (accountType.equals("SAVINGS")) {

            fee.setPercentage(BigDecimal.valueOf(1.25).setScale(2, RoundingMode.HALF_EVEN));

        } else if (accountType.equals("CHECKING")) {

            fee.setPercentage(BigDecimal.valueOf(1.50).setScale(2, RoundingMode.HALF_EVEN));

        } else {
            fee.setPercentage(BigDecimal.valueOf(2).setScale(2, RoundingMode.HALF_EVEN));
        }

        final BigDecimal feeAmount = fee.getPercentage().divide(
                BigDecimal.valueOf(100),
                RoundingMode.HALF_DOWN).multiply(BigDecimal.valueOf(amount));
        fee.setFeeAmount(feeAmount.setScale(2, RoundingMode.HALF_EVEN));

        fee.setTotalAmount(
                BigDecimal.valueOf(amount).add(fee.getFeeAmount()).setScale(2, RoundingMode.HALF_EVEN)
        );

        return Optional.of(fee);
    }
}
