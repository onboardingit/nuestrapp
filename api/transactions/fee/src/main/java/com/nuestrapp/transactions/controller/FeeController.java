package com.nuestrapp.transactions.controller;

import com.nuestrapp.transactions.domain.Fee;
import com.nuestrapp.transactions.domain.dto.FeeInput;
import com.nuestrapp.transactions.domain.dto.FeeOutput;
import com.nuestrapp.transactions.domain.mapper.FeeMapper;
import com.nuestrapp.transactions.service.FeeService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController()
@RequestMapping("")
public class FeeController {

    @Autowired
    private FeeService feeService;

    @Autowired
    private FeeMapper feeMapper;

    @ApiOperation("Permite monitorear el estado de disponibilidad del API")
    @ApiResponse(code = 200, message = "El API se encuentra disponible", response = String.class)
    @GetMapping("/health-check")
    public ResponseEntity<String> healthCheck() {
        return new ResponseEntity("Hello! i'm OK", HttpStatus.OK);
    }


    @ApiOperation("Calcula los impuestos para una transacción en función del tipo de cuenta")
    @ApiResponses({
            @ApiResponse(code = 409, message = "Los impuestos no pudieron ser calculados", response = String.class),
            @ApiResponse(code = 200, message = "El calculo de los impuestos", response = FeeOutput.class)
    })
    @PostMapping(path = "", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity calculateTransactionFee(
            @ApiParam(
                    required = true,
                    value = "Recibe la especificación del monto a transaccionar y el tipo de cuenta",
                    example = "{\"account_type\": \"SAVINGS\",\"amount\" : 2345}"
            )
            @RequestBody FeeInput requestBody
    ) {
        try {
            Fee fee = feeService.calculateTransactionFee(
                    requestBody.getAccountType(),
                    requestBody.getAmount()).get();

            return new ResponseEntity<FeeOutput>(feeMapper.toFeeOutput(fee), HttpStatus.OK);

        } catch (Exception error) {
            return new ResponseEntity<String>("No se pudo calcular el impuesto de la transacción", HttpStatus.CONFLICT);
        }
    }
}
