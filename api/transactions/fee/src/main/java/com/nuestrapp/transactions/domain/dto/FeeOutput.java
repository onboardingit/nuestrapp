package com.nuestrapp.transactions.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
public class FeeOutput {

    @Getter
    @Setter
    @NonNull
    @JsonProperty("total_amount")
    private BigDecimal totalAmount;

    @Getter
    @Setter
    @NonNull
    private BigDecimal percentage;

    @Getter
    @Setter
    @NonNull
    @JsonProperty("fee_amount")
    private BigDecimal feeAmount;
}
