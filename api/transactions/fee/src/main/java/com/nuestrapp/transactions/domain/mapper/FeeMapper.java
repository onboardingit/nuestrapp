package com.nuestrapp.transactions.domain.mapper;

import com.nuestrapp.transactions.domain.Fee;
import com.nuestrapp.transactions.domain.dto.FeeOutput;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface FeeMapper {

    @Mappings({
            @Mapping(source = "totalAmount", target = "totalAmount"),
            @Mapping(source = "percentage", target = "percentage"),
            @Mapping(source = "feeAmount", target = "feeAmount")
    })
    FeeOutput toFeeOutput(Fee fee);
}
