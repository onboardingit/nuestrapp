package com.nuestrapp.transactions.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class FeeInput {

    @Getter
    @Setter
    @JsonProperty("account_type")
    private String accountType;

    @Getter
    @Setter
    private double amount;
}
