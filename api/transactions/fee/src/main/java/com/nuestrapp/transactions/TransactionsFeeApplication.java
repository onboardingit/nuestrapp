package com.nuestrapp.transactions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransactionsFeeApplication {

    public static void main(String[] args) {
        SpringApplication.run(TransactionsFeeApplication.class, args);
    }

}
