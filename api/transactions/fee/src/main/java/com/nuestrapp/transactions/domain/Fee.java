package com.nuestrapp.transactions.domain;

import lombok.*;

import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
public class Fee {

    @Getter
    @Setter
    @NonNull
    private BigDecimal totalAmount;

    @Getter
    @Setter
    @NonNull
    private BigDecimal percentage;

    @Getter
    @Setter
    @NonNull
    private BigDecimal feeAmount;
}
