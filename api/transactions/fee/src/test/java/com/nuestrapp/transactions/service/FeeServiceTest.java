package com.nuestrapp.transactions.service;

import com.nuestrapp.transactions.domain.Fee;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FeeServiceTest {

    private FeeService service = new FeeService();

    @Test
    void calculateTransactionFeeWhenAccountTypeSavings() {
        Fee result = service.calculateTransactionFee("SAVINGS", 1100).get();

        assertEquals(result.getFeeAmount().toString(), "11.00");
        assertEquals(result.getPercentage().toString(), "1.25");
        assertEquals(result.getTotalAmount().toString(), "1111.00");

    }

    @Test
    void calculateTransactionFeeWhenAccountTypeChecking() {
        Fee result = service.calculateTransactionFee("CHECKING", 5634).get();

        assertEquals(result.getFeeAmount().toString(), "56.34");
        assertEquals(result.getPercentage().toString(), "1.50");
        assertEquals(result.getTotalAmount().toString(), "5690.34");

    }

    @Test
    void calculateTransactionFeeWhenAccountTypeUnknown() {
        Fee result = service.calculateTransactionFee("LOAN", 977).get();

        assertEquals(result.getFeeAmount().toString(), "19.54");
        assertEquals(result.getPercentage().toString(), "2.00");
        assertEquals(result.getTotalAmount().toString(), "996.54");

    }
}